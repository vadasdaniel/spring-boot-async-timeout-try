package hu.dan.timeouttry.util;

import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

@Service
public class FileResponseUtil {

    public static final String APPLICATION_ZIP = "application/zip";
    private static final String HEADER_VALUE = "attachment; filename=";

    public ResponseEntity<Resource> createResponse(File file, String fileName) throws FileNotFoundException {
        HttpHeaders header = new HttpHeaders();
        header.add(HttpHeaders.CONTENT_DISPOSITION, HEADER_VALUE + fileName);

        InputStreamResource resource = new InputStreamResource(new FileInputStream(file));

        return ResponseEntity.ok()
                .headers(header)
                .body(resource);
    }

    public ResponseEntity<Resource> createResponse(File file, String fileName, String contentType) throws FileNotFoundException {
        HttpHeaders header = new HttpHeaders();
        header.add(HttpHeaders.CONTENT_DISPOSITION, HEADER_VALUE + fileName);
        header.add(HttpHeaders.CONTENT_TYPE, contentType);

        InputStreamResource resource = new InputStreamResource(new FileInputStream(file));

        return ResponseEntity.ok()
                .headers(header)
                .body(resource);
    }

    public HttpHeaders createHeader(String fileName) {
        HttpHeaders header = new HttpHeaders();
        header.add(HttpHeaders.CONTENT_DISPOSITION, HEADER_VALUE + fileName);
        return header;
    }

    public HttpHeaders createHeader(String fileName, String contentType) {
        HttpHeaders header = new HttpHeaders();
        header.add(HttpHeaders.CONTENT_DISPOSITION, HEADER_VALUE + fileName);
        header.add(HttpHeaders.CONTENT_TYPE, contentType);

        return header;
    }

}
