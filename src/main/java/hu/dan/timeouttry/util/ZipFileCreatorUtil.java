package hu.dan.timeouttry.util;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Service
public class ZipFileCreatorUtil {

    public void createZip(OutputStream out) throws IOException {

        ZipOutputStream zipOutputStream = new ZipOutputStream(out);
        zipOutputStream.setLevel(Deflater.BEST_SPEED);

        zipOutputStream.putNextEntry(new ZipEntry("win7_2.utm"));
        InputStream fileFromDetails = getFileFromDetails();
        IOUtils.copy(fileFromDetails, zipOutputStream);

        fileFromDetails.close();
        zipOutputStream.closeEntry();
        zipOutputStream.close();
    }

    private InputStream getFileFromDetails() throws IOException {
        File file = new File("/Users/vadasdaniel/Downloads/Windows72.utm.zip");
        return new InputStreamResource(new FileInputStream(file)).getInputStream();
    }
}
