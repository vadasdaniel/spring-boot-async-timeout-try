package hu.dan.timeouttry.service;

import hu.dan.timeouttry.util.ZipFileCreatorUtil;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.OutputStream;

@Service
public class TimeOutService {

    private final ZipFileCreatorUtil zipFileCreatorUtil;

    public TimeOutService(ZipFileCreatorUtil zipFileCreatorUtil) {
        this.zipFileCreatorUtil = zipFileCreatorUtil;
    }

    public void getUploadedDocsZip(OutputStream out) throws IOException {
        zipFileCreatorUtil.createZip(out);
    }
}
