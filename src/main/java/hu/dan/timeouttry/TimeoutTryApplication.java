package hu.dan.timeouttry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TimeoutTryApplication {

    public static void main(String[] args) {
        SpringApplication.run(TimeoutTryApplication.class, args);
    }

}
