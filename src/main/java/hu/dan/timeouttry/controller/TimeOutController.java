package hu.dan.timeouttry.controller;

import hu.dan.timeouttry.service.TimeOutService;
import hu.dan.timeouttry.util.FileResponseUtil;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import java.time.temporal.ChronoUnit;

@RestController
public class TimeOutController {

    private FileResponseUtil fileResponseUtil;
    private TimeOutService service;

    public TimeOutController(FileResponseUtil fileResponseUtil, TimeOutService service) {
        this.fileResponseUtil = fileResponseUtil;
        this.service = service;
    }

    @GetMapping(value = "/streamingbody", produces = "application/zip")
    public ResponseEntity<StreamingResponseBody> first() {

        String fileName = "";
        return ResponseEntity
                .ok()
                .headers(fileResponseUtil.createHeader(fileName, "application/zip"))
                .body(out -> service.getUploadedDocsZip(out));

    }

    @GetMapping("/shouldfail")
    public String shouldFail() throws InterruptedException {
        Thread.sleep(65000);
        System.out.println("helo");
        return "JAJ";
    }
}


// octent

//callable

//steaming

//responsebody